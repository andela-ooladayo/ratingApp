'use strict';

var _ = require('lodash'),
    db = require('../../config/sequelize'),
    mailer = require('./email.server.controller'),
    roleManager = require('../../config/roleManager'),
    errorHandler = require('./errors');


exports.requestToBeMerchant = requestToBeMerchant;
exports.approvalToBeMerchant = approvalToBeMerchant;
exports.waitingList = waitingList;



function requestToBeMerchant(req, res) {
    var requestApproval = {};

    requestApproval.user_id = req.user.id;
    var msg = {};

    db.merchant_wating_approval.create(requestApproval).then(function() {

        msg.subject = "Request to be a Merchant";
        msg.from = "no-reply@onepercentlab.com";
        msg.to = req.user.email;
        msg.html = '<body>' +
                    // '< style type = "text/css" > ' +
                    // '@import url(http://fonts.googleapis.com/css?family=Raleway);' +
                    // '</style>' +
                    '<div style="; color: #515151; ' +
                    'margin: 2%; background: #81AEBA; width: 94%; font: 1.1em Raleway, Trebuchet Ms; line-height: 26px; ' +
                    'padding-bottom: 15px;">' +
                    '<div style="height: 70px; text-align: center; padding: 22px; background-color: #C7DFE8;">' +
                    '<a style="{ color: #458596; text-decoration: none; } :hover{ text-decoration: underline }" ' +
                    'href="http://www.raytee.com" target="_blank">' +
                    '<img src="https://s3-us-west-2.amazonaws.com/raytee/raytee.png" style="width: 200px;">' +
                    '</a>' +
                    '</div>' +
                    '<div style="width: 80%; background: white; margin: 15px auto 0; padding: 20px;">' +
                    '<h2>' +
                    'Hello ' + req.user.firstname + ',' +
                    '</h2>' +
                    '<hr style="border: 1px solid #ccc; margin: 1em 0;">' +
                    '<p>' +
                    ' This is a merchant request for your account.' +
                    '</p>' +
                    '<p>' +
                    ' On approval, we will send you a confirmation email.' +
                    '</p>' +
                    '<p>' +
                    'We Make It Simpler.' +
                    '</p>' +
                    '<p>' +
                    'Raytee is your voice. Use it.' +
                    '</p>' +
                    '<p>' +
                    'Thank You.' +
                    '</p>' +
                    '<p>' +
                    'The Raytee Team' +
                    '</p>' +
                    '</div>' +
                    '<div style="width: 80%; background: white; margin: 15px auto 0; padding: 10px 20px; font-size: 0.9em; text-align: center;">' +
                    '<span style="display:block;">' +
                    '&copy; 2016 Raytee  Team </span>' +
                    '</div>' +
                    '</div>' +
                    '</body>';
        mailer(msg);

        return res.status(200).json({message : "Request for Merchant approval sent"});
    }, function(err) {
        return res.status(400).send({
            message: errorHandler.getErrorMessage(err)
        });
    });
};


function approvalToBeMerchant(req, res) {
    db.User.find({where: { id: req.body.user_id } }).then(function(user) {
        if (!user) {
            return res.status(400).json({
                message: errorHandler.getErrorMessage(new Error('Failed to load user ' + id))
            });
        }
        var newBody = {};
        newBody.roleTitle = roleManager.userRoles.merchant.title;
        newBody.roleBitMask = roleManager.userRoles.merchant.bitMask;

        user = _.extend(user, newBody);
        user.updated = Date.now();
        user.save().then(function () {

            db.merchant_wating_approval.find({where: { user_id: user.id } }).then(function(merchantWaiting) {
                merchantWaiting.destroy().then(function() {
                    var msg = {};
                    msg.subject = "Congratulations!!!";
                    msg.from = "no-reply@onepercentlab.com";
                    msg.to = user.email;
                    msg.html = '<body>' +
                                // '<style type="text/css">' +
                                // '@import url(http://fonts.googleapis.com/css?family=Raleway);' +
                                // '</style>' +
                                '<div style="; color: #515151; ' +
                                'margin: 2%; background: #81AEBA; width: 94%; font: 1.1em Raleway, Trebuchet Ms; line-height: 26px; ' +
                                'padding-bottom: 15px;">' +
                                '<div style="height: 70px; text-align: center; padding: 22px; background-color: #C7DFE8;">' +
                                '<a style="{ color: #458596; text-decoration: none; } :hover{ text-decoration: underline }" ' +
                                'href="http://www.raytee.com" target="_blank">' +
                                '<img src="https://s3-us-west-2.amazonaws.com/raytee/raytee.png" style="width: 200px;">' +
                                '</a>' +
                                '</div>' +
                                '<div style="width: 80%; background: white; margin: 15px auto 0; padding: 20px;">' +
                                '<h2>' +
                                'Hello ' + user.firstname + ',' +
                                '</h2>' +
                                '<hr style="border: 1px solid #ccc; margin: 1em 0;">' +
                                '<p>' +
                                'Your merchant request has been approved.' +
                                '</p>' +
                                '<p>' +
                                'Create Memorable Customer Experiences And Witness Sustained Business Growth.' +
                                '</p>' +
                                '<p>' +
                                'We Make It Simpler.' +
                                '</p>' +
                                '<p>' +
                                'Thank You.' +
                                '</p>' +
                                '<p>' +
                                'The Raytee Team' +
                                '</p>' +
                                '</div>' +
                                '<div style="width: 80%; background: white; margin: 15px auto 0; padding: 10px 20px; font-size: 0.9em; text-align: center;">' +
                                '<span style="display:block;">' +
                                '&copy; 2016 Raytee  Team </span>' +
                                '</div>' +
                                '</div>' +
                                '</body>';
                    mailer(msg);

                    req.login(user, function (err) {
                        if (err) {
                            res.status(400).json(err);
                        } else {
                            res.status(200).json({message : user.firstname + " given merchant access"});
                        }
                    });
                }, function(err) {
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err)
                    });
                }); 
            });
            
        }, function(err) {
            return res.status(400).json({
                message: errorHandler.getErrorMessage(err)
            });
        });
    }, function(err) {
        return res.status(400).json({
            message: errorHandler.getErrorMessage(err)
        });
    });
} 


function waitingList(req, res) {
   db.merchant_wating_approval.findAll().then(function(waitingRequests) {
        var completeWaitingList = [];
        var waitingRequestlen = waitingRequests.length;

        _.forEach(waitingRequests, function (waitingRequest, key) {
            db.User.find({where: { id: waitingRequest.user_id } }).then(function(user) {
                if(user) {
                    waitingRequest.dataValues.firstname = user.firstname || "";
                    waitingRequest.dataValues.lastname = user.lastname || "";
                    waitingRequest.dataValues.phone_number = user.phone_number || "";
                    waitingRequest.dataValues.email = user.email || "";
                    completeWaitingList.push(waitingRequest);
                }
                else {
                    completeWaitingList.push(waitingRequest);
                }

                if((key + 1) == waitingRequestlen) {
                     return res.status(200).json(completeWaitingList);
                }
            }, function(err) {
                return res.status(400).json({
                    message: errorHandler.getErrorMessage(err)
                });
            });
        });
    }, function(err) {
        return res.status(400).json({
            message: errorHandler.getErrorMessage(err)
        });
    }); 
}

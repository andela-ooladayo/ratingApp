'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication', '$http', 'ServiceFac', 'User', 'Message',
	function($scope, Authentication, $http, ServiceFac, User, Message) {
		// This provides Authentication context.
    $scope.user = User.get();
		$scope.authentication = Authentication;
    $scope.isAuthenticated = Authentication.isAuthenticated();
    
    $scope.categories = [
      { name: "Agro-Allied", image: "/modules/core/img/agric.jpg", id: '0' },
      { name: "Banking & Finance", image: "/modules/core/img/biz_service.jpg", id: '1' },
      { name: "Business Services", image: "/modules/core/img/business.jpg", id: '2' },
      { name: "ICT", image: "/modules/core/img/ict.jpg", id: '3' },
      { name: "Educational", image: "/modules/core/img/edu.jpg" , id: '4' },
      { name: "Events & Entertainment", image: "/modules/core/img/events.jpg", id: '5' },
      { name: "Nightlife and clubs", image: "/modules/core/img/nightlife.jpg", id: '6' },
      { name: "Fashion & Style", image: "/modules/core/img/fashion.jpg", id: '7' },
      { name: "Food & Beverages", image: "/modules/core/img/food.jpg", id: '8' },
      { name: "Restaurants", image: "/modules/core/img/restaurant.jpg", id: '22' },
      { name: "Hotels", image: "/modules/core/img/hotels.jpg", id: '21' },
      { name: "Manufacturing", image: "/modules/core/img/construction.jpg", id: '10' }
    ];


    ServiceFac.getTopRated().then(function(data) {
      $scope.topRated = data.data;
    })

    ServiceFac.getTopReviews().then(function(data) {
        $scope.topReviews = data;
    })

    $scope.sendmessage = function() {
      if ($scope.sender ===  undefined || $scope.sender_email === undefined || $scope.sender_message === undefined) {
        Message.warning("Fill the form accurately");
        return;
      }
      var msg = {
        name: $scope.sender,
        email: $scope.sender_email,
        message: $scope.sender_message
      }

      $http.post('/post_email', msg).success(function(response) {
          Message.success('Contact us', response.message);
          $scope.sender = '';
          $scope.sender_email = '';
          $scope.sender_message = '';
      }).error(function(response) {
        // console.log(response);
      })
    }


    $("#top").backstretch(["/modules/core/img/bg2.jpg", "/modules/core/img/bg1.jpg", "/modules/core/img/bg3.jpg"], {duration:3000, fade: 750});
	}
]);
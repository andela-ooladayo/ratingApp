'use strict';

angular.module('users').controller('DashboardController', ['$scope', '$location', 'Users', 'Authentication','User', 'Message', 'Merchant', 'Stats',
    function($scope, $location, Users, Authentication, User, Message, Merchant, Stats) {
        $scope.user = User.get();

        // console.log($scope.user);
        // If user is not signed in then redirect back home
        if (!$scope.user) $location.path('/');


        // Todo: Add error implementation

        Stats.getStats().then(function(response) {
           $scope.stats = response;
            $scope.labels = ["Users", "Merchants", "Businesses", "Reviews"];
            $scope.data = [$scope.stats.total_users, $scope.stats.total_merchants, $scope.stats.total_services, $scope.stats.total_reviews]; 
        });

        Stats.getUsers().then(function(response) {
            $scope.users = response;
        });


        $scope.findMerchantList = function() {

            Merchant.getList().then(function(response) {
                $scope.waitingList = response;
            });
        };

        $scope.approveMerchant = function(req) {
            Merchant.approve(req);
        }


    }
]);

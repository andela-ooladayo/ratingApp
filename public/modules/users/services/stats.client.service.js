'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Stats', ['$http', '$q', 'Message',
    function($http, $q, Message) {

        var getStats = function() {
            return $http.get('/api/stats').then(function(response) {
                return response.data;
            });
        }

        var getUsers = function() {
            return $http.get('/api/users').then(function(response) {
                return response.data;
            });
        }


        return {
            getUsers: getUsers,
            getStats: getStats
        }
    }
]);
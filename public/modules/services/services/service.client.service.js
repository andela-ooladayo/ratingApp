'use strict';

//Services service used for communicating with the services REST endpoints
angular.module('services').factory('ServiceFac', ['$resource', '$http',
    function($resource, $http) {

        var getAverageRating = function(objArray) {
            for(var i = 0; i < objArray.length; i++) {
                calcAverageRating(objArray[i]);
            }
            return objArray;
        };

        var findOne = function(idx) {
            return $http.get('/service/' + idx).then(function(res) {
                return calcAverageRating(res.data);
            });
        }

        var calcAverageRating = function(obj) {
            var total = 0,
                cumm = 0,
                average = 0;
            //assumption
            var index = 5;
            for (var key in obj) {
                if ( key.indexOf("no_of_rating") >= 0 && index > 0) {
                    cumm += parseInt(obj[key]) * index;
                    total += parseInt(obj[key]);
                    index--;
                }
            }

            if (total > 0) {
                obj.avg_rating = cumm / total;
                calcPercentage(obj, total);
            } else {
                obj.avg_rating = 0;
            }
            return obj;
        }

        var calcPercentage = function(obj, total) {
            obj.percentage_five =   (parseInt(obj.no_of_rating_five) / total) * 100;
            obj.percentage_four =   (parseInt(obj.no_of_rating_four) / total) * 100;
            obj.percentage_three =   (parseInt(obj.no_of_rating_three) / total) * 100;
            obj.percentage_two =   (parseInt(obj.no_of_rating_two) / total) * 100;
            obj.percentage_one =   (parseInt(obj.no_of_rating_one) / total) * 100;
        }

        var getTopReviews = function() {
            var reviews = [];
            return $http.get('/service/top-reviews').then(function(response) {
                response.data.data.forEach(function(obj) {
                    findOne(obj.service_id).then(function(data) {
                        obj.img = data.images[0].url;
                        reviews.push(obj);
                    });
                });
                return reviews;
            });
        }

        var getTopRated = function() {
            return $http.get('/service/top-rated').then(function(response) {
                return response.data;
            });
        }

        var getByCategory = function(idx) {
            return $http.get('/service/category/' + idx + '?nums=30').then(function(response) {
                return getAverageRating(response.data);
            })
        }

        var getAllBusinesses = function() {
            return $http.get('/service').then(function(response) {
                return getAverageRating(response.data);
            });
        };

        return {
            findOne: findOne,
            getTopReviews: getTopReviews,
            getTopRated: getTopRated,
            getByCategory: getByCategory,
            getAllBusinesses: getAllBusinesses
        }
    }
]);


'use strict';

angular.module('services').controller('CategoriesController', ['$scope', '$stateParams', '$window', '$location', '$http', 'User', 'Authentication','Message', 'Storage', 'Services', 'ServiceFac', 'Images', 'Reviews', 'Likes',
    function($scope, $stateParams, $window, $location, $http, User, Authentication, Message, Storage, Services, ServiceFac, Images, Reviews, Likes) {

        $scope.authentication = Authentication;
        $scope.isAuthenticated = Authentication.isAuthenticated();

        $scope.categories = [
          "Agriculture & Agro-Allied",
          "Banking & Finance (banks)",
          "Business Services",
          "ICT",
          "Educational",
          "Events & Entertainment",
          "Nightlife and clubs",
          "Fashion & Style",
          "Food & Beverages",
          "Maintenance Services",
          "Manufacturing & Production",
          "Medicine & Health",
          "Pharmaceutical companies",
          "Newspapers & Media",
          "Oil & Gas",
          "People & Society",
          "Professional Services",
          "Construction/Real Estate",
          "Religious Organisations",
          "Telecommunications",
          "Travel & Tourism",
          "Hotels",
          "Restaurants",
          "Industrial Goods",
          "Consumer Goods",
          "Services",
          "Utilities"
        ];

        // $scope.categoryArr = [];

        $scope.setFilter = function(param) {
            $scope.filter = param;
        }

        ServiceFac.getByCategory($stateParams.categoryId).then(function(data) {
          $scope.category = $scope.categories[$stateParams.categoryId];
          $scope.categoryArr = data;
        });

        ServiceFac.getTopReviews().then(function(data) {
            $scope.topReviews = data;
        });


        $('#cat').on('change', function() {
            $scope.categoryArr = [];
            var val = this.value;
             $scope.category = $scope.categories[val];
            ServiceFac.getByCategory(val).then(function(data) {
                $scope.categoryArr = data;
            });
        });

    }
]);